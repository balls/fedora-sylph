# just some packages that I use


%packages



# #
npm


# i3 wm
i3
i3status
dmenu
i3lock
xbacklight
feh #image viewer
lightdm # a DM for just sake of it
nitrogen # to set wallpaper for X

# some additional stuff for i3wm
numlockx
volumeicon
xfce4-power-manager 
powerline 
powerline-fonts




#tools
conky
htop
cowsay #for the lulz
nmon
nethogs
p7zip
p7zip-gui
gparted
@admin-tools
wget
rfkill
screen
fpaste
#terminator #terminal is fine for now 
powerline
powerline-fonts
powertop # as name says, power top
testdisk #u ll never know when u ll need it
file-roller

#text 
emacs
emacs-auto-complete
geany
#leafpad #one too many text editors
meld


#for internet & office
midori # I am keeping it
firefox # Default
links # for the lulz 
git
mosh
libreoffice
qbittorrent

telegram-desktop

tor
torbrowser-launcher


#media
gimp
audacity
easytag
youtube-dl

#l10n
langpacks-ta
ibus-setup
ibus-m17n


#sync
kde-connect # to pair with android


#XFCE components #might already be included, but dont want to miss
f25-backgrounds-xfce
xfce4-screenshooter-plugin
xfce4-whiskermenu-plugin
xfce4-systemload-plugin
xfce4-cellmodem-plugin
xfce4-mailwatch-plugin
xfce4-datetime-plugin
xfce4-sensors-plugin
xfce4-weather-plugin
xfce4-power-manager
xfce4-dict-plugin
xfdashboard-themes
xfdashboard


# Py
python-devel 
python3-devel

# Games ## HELL YEAH
bastet
ninvaders
nsnake
bsd-games
moon-buggy

## remove these 
-@xfce-media



%end
